//JSON OBJECTS
/*
	-It stands for JavaScript Object Notation
	-Also used in other programming languages
	-Core Javascript has a built in JSON Object that contains method for parsing JSON objects and converting strings into Javascript objects
	-Javascript objects are not to be confused with JSON
	-JSON is used for serializing different data types to bytes
	-Serialization is the process of converting data into a series of bytes for easier transmission / transfer of information
	Syntax:
		{
			"propertyA": "propertyA",
			"propertyB": "propertyB"
		}
*/

//JSON

/*{
	"city": "Quezon City",
	"province": "Metro Manila",
	"country": "Philippines"
}*/


//JSON ARRAY

/*"cities": [
	{"city": "Quezon City", "province": "Metro Manila", "country": "Philippines"},
	{"city": "Manila City", "province": "Metro Manila", "country": "Philippines"},
	{"city": "Makati City", "province": "Metro Manila", "country": "Philippines"},
]

console.log(cities[0].city) //this cannot be accessed
*/

//PARSE: Converting JSON string into objects

let batchesJSON = `[{"batchName": "Batch X"}, {"batchName": "Batch Y"}]`;

console.log("Result from parse method:")
console.log(JSON.parse(batchesJSON));

console.log(batchesJSON[0]);


let stringifiedObject = '{"name": "Eric", "age": "9", "address": {"city": "Bonifacio Global City", "country": "Philippines"}}'
console.log(JSON.parse(stringifiedObject));
console.log("");

//Stringify: Convert Objects into String(JSON)
//JSON.stringify
let data = {
	name: "Gabryl",
	age: 61,
	address: {
		city: "New York",
		country: "USA"
	}
}

console.log(data);
console.log(typeof data);

let stringData = JSON.stringify(data);
console.log(stringData);
console.log(typeof stringData);





















